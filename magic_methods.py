class Garage:
    def __init__(self):
        self.car = []

    def __len__(self):
        return len(self.car)

    def __getitem__(self, i):
        return self.car[i]

    def __repr__(self):
        return f'<Garage {self.car}>'

    def __str__(self):
        return f'Garage with {len(self)} cars.'

ford = Garage()
ford.car.append('toyota')
ford.car.append('maruthi')
print(ford.car)
print(ford)
print(ford[1])
for car in ford:
    print(car)
