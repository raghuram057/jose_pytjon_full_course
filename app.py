"""
-"Enter 'a' to add a movies, 'l' to see a movies, 'f' to find a movies, 'q' to quit :"

-Add movies
-List movies
-Find a Movie
-Stop running the program

Tasks:
[]:Decide where to store movies
[]:What is the format of movies?
[]:Show the user main interface and get the user input
[]:Allow user to add movies
[]:Show all their movies
[]:Stop running the program when they type 'q'

"""
movies = []
"""
movie = {
    'name': ........(str)
    'director':.....(str)
    'year':.........(int)
    }
"""


def menu():
    user_input = input("Enter 'a' to add a movies, 'l' to see a movies, 'f' to find a movies, 'q' to quit :")

    while user_input != 'q':

        if user_input == 'a':
            add_movie()
        elif user_input == 'l':
            show_movies(movies)
        elif user_input == 'f':
            find_movie()
        else:
            print('Invalid user input, try again!!!')
        user_input = input("\nEnter 'a' to add a movies, 'l' to see a movies, 'f' to find a movies, 'q' to quit :")


def add_movie():
     movie_name = input('Enter the movie name :')
    movie_director = input(f'Enter the {movie_name} director name :')
    release_date = input(f'Enter {movie_name} release date :')

    # movie = {'movie': movie_name,'director': movie_director,'year': release_date}
    movies.append({
        'movie': movie_name,
        'director': movie_director,
        'year': release_date
    })


def show_movies(movie_list):
    for movie in movie_list:
        show_movie_details(movie)


def show_movie_details(movie):
    print(f"Name : {movie['movie']}")
    print(f"Director_Name : {movie['director']}")
    print(f"Release_Date : {movie['year']}")


def find_movie():
    find_by = input('What property of the movie you are looking for(movie? director? year? ?')
    looking_far = input('What are you searching for ?')

    found_movies = find_by_attributes(movies,looking_far,lambda x:x[find_by])
    show_movies(found_movies)


def find_by_attributes(items,expected,finder):
    found = []
    for i in items:
        if finder(i) == expected:
            found.append(i)
    return found


menu()
